#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Función que elimina el caracter 'c' de un texto
void eliminarLetra(){ 
	char* texto     = "cada dia carmen salia a la calle"; //se define el texto que se usará
	char* textoSinC = calloc (33, sizeof(char));          //se guarda espacio en el heap para la variable textoSinC
	char  caracter  = 'c'; 							      //se da el valor del caracter que se va a eliminar
	char  borrar    = ' ';								  //se le da el valor de un espacio en blanco a la variable borrar
	int   i         = 0;
	while (texto[i] != '\0'){					          //se da la condición de parada del ciclo que sería cuando se llegue al '\0' en el texto
		
		if (texto[i] == caracter){					      //se comparan las letras para ver si es una c
			textoSinC[i] = borrar;					      //si fuera el caso se remplaza la c por un espacio en blanco
		}
		else{
			textoSinC[i] = texto[i];				      //si es lo contrario se le sigue añadiendo a textoSinC las letras que siguen
		}
		i++;										      //se le añada uno a i para que no sea un ciclo infinito
	}
	printf("El texto 'cada dia carmen salia a la calle' quedaría así: %s\n",textoSinC); //por último se imprime el texto con la c y el texto sin la c

}

//Función que invierte un texto
void invertirPalabra(){
	char* texto     = "ese casco esta grande";         //se define el texto que se usará 
	char* invertido = calloc (22, sizeof(char));       //se guarda espacio en el heap para la variable invertido
	int   i         = 0;
	int   largo     = strlen(texto);				   //se defina a la variable largo,que es el largo del texto
	while(texto[i] != '\0'){						   //se da la condición de parada del ciclo que sería cuando se llegue al '\0' en el texto
		
		invertido[i] = texto[largo-1];                 //se va dando el valor de las letras invertidas
		largo--;									   //se le va restando una unidad a largo 
		i++;										   //se la añada a i uno para que no sea un ciclo infinito
	}
	
	printf("El texto '%s' de manera invertida quedaría así:\n %s\n",texto,invertido); //por último se imprime el texto normal junto con el texto invertido
}

//Función que copia un texto literal a un texto en el heap
void literalHeap(){
	char* texto     = "ese casco está grande";                  //se define el texto literal
	char* textoHeap = calloc(22, sizeof(char));					//se guarda en el espacio heap para la variable textoHeap
	
	strcpy(textoHeap, texto);                                   //se copia lo que estaba en la variable texto en la variable textoHeap
	
	printf("el texto que queda en el heap sería: %s\n", textoHeap); //se imprime el texto final
}

//Función principal que llama a las otras
int main(int argc, char const *argv[]){
	int opcion = 0;                       //se define la variable opcion
	printf ("Ingrese el problema que se desea resolver. \n1: Eliminar el caracter c en una palabra. \n2: Invertir un texto. \n3: Copiar un texto literal a un texto en el heap \n"); //se le imprimen una serie de opciones al usuario
	scanf  ("%d",&opcion);   //se guarda en la variable opcion la que escogió el usuario
	if (opcion == 1){		 //si la opción escogida fue la número 1, se llama a la función eliminarLetra
		eliminarLetra();
	}
	if (opcion == 2){		 //si la opción escogida fue la número 2, se llama a la función invertirPalabra
		invertirPalabra();
	}
	if (opcion == 3){		 //si la opción escogida fue la número 3, se llama a la función literalHeap
		literalHeap();
	}
	return 0;
}
