#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//Estructura de cada nodo de la lista
struct nodo_lista
{
	int vida;                //vida de la nave      
	int num_jugador;         //número de jugador (1 o 2)        
	int potencia;            //potencia de disparos de la nave
	int identificador;       //identificador de la nave (van del 1 al 6)
	char *nave;			     //representación de la nave con símbolos
	struct nodo_lista *sig;  //el puntero a siguiente
	struct nodo_lista *ant;  //el puntero a anterior    
};

//Estructura de la lista
struct lista{
	struct nodo_lista* inicio;            //se hace con el inicio
};

//Creación de la lista reservando memoria
struct lista* crear_lista(){
	struct lista *nueva_lista = calloc(1,sizeof(struct lista));
	return nueva_lista; 
}

//Creación de los nodos
struct nodo_lista* crear_nodo(){
	struct nodo_lista *nuevo_nodo = calloc(1,sizeof(struct nodo_lista));
	return nuevo_nodo;
}
//Función para ir insertando las naves en la lista
struct lista* insertar(struct lista* lista_naves,char *nombre, int identificador, int vida, int potencia){
	struct nodo_lista* nuevo_nodo = NULL;
	struct nodo_lista* actual = NULL;
	 
	if (lista_naves == NULL){ //Si no hay lista
		return NULL;
	}

	nuevo_nodo = crear_nodo(); 
	if(nombre=="<(-)>"){				//Depende de la forma de la nave se asignará al jugador que pertenece
		nuevo_nodo->num_jugador = 1;
	}
	if(nombre=="}<x>{"){
		nuevo_nodo->num_jugador = 2;
	} 
	nuevo_nodo->vida = vida;					//se asignan los otros elementos del nodo
	nuevo_nodo->potencia=potencia;
	nuevo_nodo->nave = nombre;
	nuevo_nodo->identificador = identificador;

	if (lista_naves->inicio == NULL){     //Si no ha un nodo ocupando el primer espacio, si está vacío.
		lista_naves->inicio = nuevo_nodo; //El nodo nuevo toma la posicion de primero en la lista.
		nuevo_nodo->sig = NULL;
		nuevo_nodo->ant=NULL;
	}else{
		actual = lista_naves->inicio;
		while(actual->sig != NULL){       //Este while va a recorrer los nodos hasta que llegue al último.
			actual = actual->sig;
		}
		actual->sig = nuevo_nodo;
		nuevo_nodo->sig = NULL;
		nuevo_nodo->ant = actual;
	}
	return lista_naves;
}

//Función que imprime las naves, con su vida, potencia e identificador de la nave
int imprimir_lista(struct lista* lista_naves){
	struct nodo_lista* actual = NULL;

	if (lista_naves == NULL){               //En caso de que la lista no ha sido creada
		printf("No hay lista\n");
		return -1;
	}
		if (lista_naves->inicio == NULL){    //En caso de que la lista esté vacía
		printf("Lista vacía\n");
		return -2;
	}

	actual = lista_naves->inicio;        //Definimos a actual como el primer nodo de la lista para imprimir en orden

	while (actual != NULL){    			 //Mientras actual no sea nulo
		printf(" %s\t\t", actual->nave); 
		actual = actual->sig;            //Se sigue definiendo a actual con el siguiente hasta llegar al final
	}
	actual = lista_naves->inicio; //Se repite el mismo proceso para imprimir la información de las naves en la siguiente línea
	printf ("\n");
	while (actual != NULL){    
		printf("%d %d %d\t\t",actual->vida,actual->potencia,actual->identificador); 
		actual = actual->sig;                   
	}
	printf ("\n\n");
	return 0;
}


//Función que borra un nodo de la lista
struct lista* borrar(struct lista *lista_naves,int identificador){   
	struct nodo_lista* actual = NULL; 
	struct nodo_lista* anterior = NULL;
	

	if (lista_naves == NULL){          //Ve si la lista no ha sido creada.
		return NULL;
	}

	if (lista_naves->inicio == NULL){   // Si la lista esta vacia
		return NULL;
	}
	actual = lista_naves->inicio;
	if(actual->identificador == identificador){  //En caso de que el nodo a borrar sea el primero
		lista_naves->inicio=actual->sig;
		free(actual);
		actual = NULL;
		return lista_naves;
		}
	while(actual!=NULL){
		if(actual->identificador==identificador){
			actual->ant->sig=actual->sig;
			actual->sig->ant=actual->ant;
			free(actual);
			actual = NULL;
			return lista_naves;
		}
		actual=actual->sig;
	}

}	

//Función para buscar una nave en la lista
struct lista* buscar_nave(struct lista* lista_naves, int identificador){   //Se define la función con sus parámetros que serían la lista y el identificador ingresado a buscar
	struct nodo_lista* actual = NULL;
	if (lista_naves == NULL || lista_naves->inicio == NULL){     //En caso de que la lista no tenga nada
		return NULL;
	}
	actual = lista_naves->inicio;         //Se define actual como el primero elemento de la lista

	while(actual != NULL){      //Mientras actual no sea nulo
		if(actual->identificador == identificador){
			return actual;
		}
		actual = actual->sig;
	}
	return NULL;
	
}

//Función principal en la cual se invocan a las otras y se hace un tipo menú para la interacción con el usuario
int main(int argc, char const *argv[]){
	struct nodo_lista* nodo_nave = NULL; //esta variable ayudará para hacer las búsquedas cuando se ingresa una nave para atacar
	int nave = NULL; 				     //en esta variable se registra el número de nave que ingresó el usuario (con la que quiere atacar)
	int ganador = 0;				     //variable que ayuda para determinar si hay o no un ganador
	int jugador1 = 3;					 //estas dos variables de jugadores son las que determinan cuantas naves ha perdido cada jugador
	int jugador2 = 3;
	char *opcion = NULL;				 
	struct lista* lista_naves = crear_lista();
	for(int i=1;i<7;i++){				//se hace un ciclo para las inserciones
		int num = rand()%11+5;			//se saca la potencia del disparo de las naves de manera aleatoria
		int numero = i;					//el siguiente bloque es para determinar si es par o impar el número, para así ir asignando las naves que le toca a cada jugador
		numero = numero%2;
		if(numero == 0){
			insertar(lista_naves,"}<x>{",i,20,num);
		}
		else{
			insertar(lista_naves,"<(-)>",i,20,num);
		}
	}
	printf("\n\t\t\t<<Bienvenido al juego amigo-enemigo>>\n\n¿Desea ver las instrucciones del juego?(y o n)\n");  //se le muestra al usuario un menú
	scanf("%s",&opcion);
	if (opcion=='y'){
		printf("\n|Este es un juego de dos jugadores. Cada jugador tiene 3 naves y consiste en atacar las naves del otro hasta derribarlas.|\n|Por turno el jugador selecciona con cual de sus naves desea atacar. La nave seleccionada atacará a las naves que tiene a|\n|su izquierda y derecha. Abajo de cada nave están el número de su vida, la potencia de su disparo y su número respectivo.|\n\n");
	}
	printf("¿Desea comenzar a jugar?(y o n)\n");
	scanf("%s",&opcion);
	if (opcion=='y'){
		printf("\nLas naves del jugador 1 son así: <(-)>\nLas naves del jugador 2 son así: }<x>{\n\n\t\t\t\t<<Comienza el juego>>\n\n");
	}
	else{
		return 0;    //si el usuario dice que no quiere jugar se sale del programa
		}
	imprimir_lista(lista_naves);
	while(ganador==0){  //ciclo para ir determinando los turnos y acciones de los jugadores
		printf("Es el turno del jugador 1, escoja el número de la nave con la que quiere atacar\n");
		scanf("%d",&nave);

		switch(nave){ 

			case 1:
			nodo_nave = buscar_nave(lista_naves,1);          //Se defina a la variable como el resultado de la búsqueda con su respectivo número, esto se hace varias veces de aquí en adelante
			if(nodo_nave->sig->vida-nodo_nave->potencia<=0){ //Condición para comprobar si una nave va a ser destruida con el disparto, esto se hace varias veces de aquí en adelante
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){  //depende de las naves a la izquierda y derecha un jugador se verá afectado
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->sig->identificador);//se borra el nodo(la nave que fue atacada y destruida)
			}else{
				nodo_nave->sig->vida = nodo_nave->sig->vida - nodo_nave->potencia; //de lo contrario solo se le resta la potencia del disparo
			}
			break;

			case 3:
			nodo_nave = buscar_nave(lista_naves,3);
			if(nodo_nave->sig->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->sig->identificador);
			}else{
				nodo_nave->sig->vida = nodo_nave->sig->vida - nodo_nave->potencia;
			}
			if(nodo_nave->ant->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->ant->identificador);
			}else{
			nodo_nave->ant->vida = nodo_nave->ant->vida - nodo_nave->potencia;
			}
			break;

			case 5:
			nodo_nave = buscar_nave(lista_naves,5);
			if(nodo_nave->sig->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->sig->identificador);
			}else{
				nodo_nave->sig->vida = nodo_nave->sig->vida - nodo_nave->potencia;
			}

			if(nodo_nave->ant->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->ant->identificador);
			}else{
			nodo_nave->ant->vida = nodo_nave->ant->vida - nodo_nave->potencia;
			}
			break;

			default:
			printf("Nave no válida\n\n");
			break;
		}

		imprimir_lista(lista_naves);

		printf("Es el turno del jugador 2, escoja el número de la nave con la que quiere atacar\n");
		scanf("%d",&nave);
			
		switch(nave){

			case 2:
			nodo_nave = buscar_nave(lista_naves,2);
			if(nodo_nave->sig->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->sig->identificador);
			}else{
				nodo_nave->sig->vida = nodo_nave->sig->vida - nodo_nave->potencia;
			}
			if(nodo_nave->ant->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->ant->identificador);
			}else{
			nodo_nave->ant->vida = nodo_nave->ant->vida - nodo_nave->potencia;
			}
			break;

			case 4:
			nodo_nave = buscar_nave(lista_naves,4);
			if(nodo_nave->sig->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;;
				}
				borrar(lista_naves,nodo_nave->sig->identificador);
			}else{
				nodo_nave->sig->vida = nodo_nave->sig->vida - nodo_nave->potencia;
			}
			if(nodo_nave->ant->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->sig->identificador == 1 || nodo_nave->sig->identificador == 3|| nodo_nave->sig->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->ant->identificador);
			}else{
			nodo_nave->ant->vida = nodo_nave->ant->vida - nodo_nave->potencia;
			}
			break;

			case 6:
			nodo_nave = buscar_nave(lista_naves,6);
			if(nodo_nave->ant->vida-nodo_nave->potencia<=0){ 
				if(nodo_nave->ant->identificador == 1 || nodo_nave->ant->identificador == 3|| nodo_nave->ant->identificador == 5){
					jugador1=jugador1-1;
				}else{
					jugador2=jugador2-1;
				}
				borrar(lista_naves,nodo_nave->ant->identificador);
			}else{
				nodo_nave->ant->vida = nodo_nave->ant->vida - nodo_nave->potencia;
			}
			break;

			default:
			printf("Nave no válida\n\n");
			break;
		}
		imprimir_lista(lista_naves);

		if (jugador1 == 0 || jugador2 == 0){ //se comprueba si hay un ganador con las variables de jugadores, si alguna tiene 0 significa que ya le destruyeron todas sus naves
			ganador = 1;
		}
	}

	if (jugador1 == 0){    //Se imprime quién fue el ganador, se imprimen cómo quedó al final y se termina el programa
		printf("\t\t\t<<El ganador es el jugador 2>>\n");
	}else{
		printf("\t\t\t<<El ganador es el jugador 1>>\n");
	}
	imprimir_lista(lista_naves);
	return 0;
}
