#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Determina si un numero es par o impar
void par(int numero){
	numero = numero%2;
	
	if(numero == 0){
		printf("Es par\n");
	}
	else{
		printf("Es impar\n");
	}
}

//Muestra la función de Fibonacci desde el 0 hasta mostar la suma de 144.
void fibonacci(){
	int numeroActual = 0;
	int numeroSucesor = 1;
	printf("%d\n%d\n", numeroActual, numeroSucesor);
	
	for (int i = 0;i<11;i++){
		int resultado = numeroActual + numeroSucesor;
		printf("%d\n",resultado);

		numeroActual = numeroSucesor; //Número actual ahora toma el valor del número sucesor.
		numeroSucesor = resultado; //Numero sucesor es el resultado de la suma de esta vuelta.
	}
}

//Muestra el factorial de un número
void factorial(){
	int numero = 0;
	printf("¿De cuál número quiere saber el factorial?\n");
	scanf("%d",&numero); 
	int resultado = 1;
	int posicion = numero; //posicion me sirve para poder restar el número sin perder el valor del numero original entrante.
	for (int i = 0;i < numero;i++){
		resultado = resultado * posicion;
		posicion = posicion - 1;
		if (numero > i+1){
			printf("%d * ", posicion+1);
		}
		else{
			printf("%d", posicion+1);
		}
	}
	printf("\nEl factorial de %d es %d\n",numero,resultado);
}


//Determina si una palabra es palindromo.
void palindromo(char* cadena){
	int largo= strlen(cadena);
	int inverso = 0;
	int comprobador = 0;
	for (int i=0;cadena[i]!='\0';i++){
		if(cadena[largo-1]!=cadena[inverso]){
			comprobador++;
		}
		inverso=inverso++;
		largo=largo--;
	}
	if(comprobador==0){
		printf("la palabra es palíndromo\n");
	}
	else{
		printf("La palabra no es palíndromo\n");
		}
}	

int largo_cadena_while(char* cadena)
{
	int largo=0;
	while (cadena[largo]!='\0') {
		largo++;
	}
	printf("El largo de la palabra es de %d letras\n", largo);
}

int largo_cadena_for(char* cadena)
{
	int largo=0;
	for (int aux=0;cadena[aux]!='\0';aux++) {
		largo++;
	}
	printf("El largo de la palabra es de %d letras\n", largo);
}

int main(int argc, char const *argv[])
{
	char* palabra = calloc (22, sizeof(char));
	int opcion = 0;
	int num = 0;
	printf("¿Cuál opción desea?\n 1- Determinar si un número es par o impar\n 2- Fibonacci\n 3- Factorial\n 4- Largo de una palabra WHILE\n 5- Largo de una palabra FOR\n 6- Determinar si una palabra es un palindromo o no\n");
	scanf("%d",&opcion);


	switch(opcion) //Switch es como un if
	{
		case 1: //Si la opción es 1
		printf("Ingrese el número que quiere probar\n");
		scanf("%d",&num);
		par(num);		
		break;

		case 2: //Si la opción es 2
		fibonacci();
		break;

		case 3: //Si la opción es 3		
		factorial(num);
		break;

		case 4:
		printf("Ingrese la palabra de la cual desea saber el largo\n");
		scanf("%s", &palabra);
		largo_cadena_while(&palabra);
		break;

		case 5: //Si la opción es 5
		printf("Ingrese la palabra de la cual desea saber el largo\n");
		scanf("%s", &palabra);
		largo_cadena_for(&palabra);
		break;

		case 6:
		printf("Ingrese la palabra de la cual desea saber el palindromo\n");
		scanf("%s", &palabra);
		palindromo(&palabra);
		break;

	}
	return 0;
}
